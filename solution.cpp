//  Copyright 2013, Nikita Kazeev
#include <iostream>
#include <fstream>

#define abs(x) ((x) < 0 ? -(x) : (x))
#define sq(X) ((X)*(X))

const int N = 101;  // Space grid spacing
// We believe the solution to converge if
// delta between steps doesn't exceed this
const double epsilon = 1e-5;

int main(int argc, char** argv) {
  const double dt = 1e-2;
  const double h = 2.0/(N - 1);

  // Thomas method
  // Kosarev p. 32
  const double A = dt/sq(h);
  const double B = -(1+2*A);

  double u[N][N];
  // The beauty of the method: it must converge,
  // however insane the initial values are.
  double delta = 2*epsilon;

  // Setting boundary conditions
  for (int m = 0; m < N; ++m) {
    // L2 is double so val is double
    double val = 1 - 4 *sq((static_cast<double>(m))/(N - 1) - 0.5);
    u[m][0] = val;
    u[m][N-1] = val;
    u[0][m] = val;
    u[N - 1][m] = val;
  }

  std::ofstream center_u_out;
  center_u_out.open("center.dat");
  while (delta > epsilon) {
    double u0 = u[N/2][N/2];
    center_u_out << u0 << " ";
    double p[N - 1];
    // p[0] must be never used
    p[1] = A/B;
    for (int i = 2; i < N - 1; ++i)
      p[i] = A/(B - p[i-1]*A);

    // First, we pass over x
    for (int l = 1; l < N - 1; ++l) {
      double q[N - 1];
      // q[0] is undefined, as we have boundary condition
      q[1] = (-u[1][l] - A*u[0][l])/B;
      for (int i = 2; i < N - 1; ++i)
        q[i] = (-u[i][l] - q[i-1]*A)/(B - p[i-1]*A);
      q[N - 2] = (-u[N - 2][l] - A*u[N - 1][l] - q[N-3]*A)/(B - p[N-3]*A);
      u[N - 2][l] = q[N - 2];
      for (int i = N - 3; i > 0; --i)
        u[i][l] = q[i] - p[i]*u[i+1][l];
    }

    // Then by y
    // Aghrhh... Cody copying, I might get crucified for that
    // Yet, there is no obvious way around...
    for (int m = 1; m < N - 1; ++m) {
      double q[N - 1];
      // q[0] is undefined, as we have boundary condition
      q[1] = (-u[m][1] - A*u[m][0])/B;
      for (int i = 2; i < N - 1; ++i)
        q[i] = (-u[m][i] - q[i-1]*A)/(B - p[i-1]*A);
      q[N - 2] = (-u[m][N-2] - A*u[m][N-1] - q[N-3]*A)/(B - p[N-3]*A);
      u[m][N-2] = q[N - 2];
      for (int i = N - 3; i > 0; --i)
        u[m][i] = q[i] - p[i]*u[m][i+1];
    }
    delta = abs(u0 - u[N/2][N/2]);
  }
  center_u_out.close();
  std::ofstream u_final_out;
  u_final_out.open("final_u.dat");
  // It's such a shame we haven't used Eigen
  for (int m = 0; m < N; ++m) {
    for (int l = 0; l < N; ++l)
      u_final_out << u[m][l] << " ";
    u_final_out << std::endl;
  }
  u_final_out.close();
  return 0;
}
